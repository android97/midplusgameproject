package buu.mongkhol.plusgameprojectbinding

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.mongkhol.plusgameprojectbinding.databinding.ActivityMainBinding
import buu.mongkhol.plusgameprojectbinding.databinding.FragmentDeleteBinding
import kotlinx.android.synthetic.main.fragment_delete.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DeleteFragment : Fragment() {

    private var deleteCorrect:Int = 0
    private var deleteIncorrect:Int = 0
    private var numberFirstDelete = Random.nextInt(0, 11)
    private var numberSeconDelete = Random.nextInt(0, 11)

    private lateinit var binding: FragmentDeleteBinding
    // TODO: Rename and change types of parameters

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentDeleteBinding>(inflater, R.layout.fragment_delete, container,false)

        binding.txtTrueDelete.setTextColor(Color.parseColor("#00ff33"))
        binding.txtFalseDelete.setTextColor(Color.parseColor("#ff33cc"))
        binding.txtTrue.setTextColor(Color.parseColor("#00ff33"))
        binding.txtFalse.setTextColor(Color.parseColor("#ff33cc"))

        randomProblem()

        return binding.root
    }

    private fun randomProblem() {
        numberFirstDelete = Random.nextInt(6, 11)
        numberSeconDelete = Random.nextInt(0, 6)
        val numberFirst = binding.txtNumberFirstDelete
        val numberSecon = binding.txtNumberSeconDelete

        numberFirst.text = "$numberFirstDelete"
        numberSecon.text = "$numberSeconDelete"
        val answer = numberFirstDelete - numberSeconDelete
        val answerString = answer.toString()

        randomAnswer(binding.btn1, binding.btn2, binding.btn3, answer)
        checkAnswer(answerString)

    }

    private fun randomAnswer(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer: Int
    ) {
        val answers: Int = Random.nextInt(0, 3)
        if (answers == 0) {
            btn1.text = (answer - 0).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer + 2).toString()
        }
        if (answers == 1) {
            btn1.text = (answer - 1).toString()
            btn2.text = (answer - 0).toString()
            btn3.text = (answer + 2).toString()
        }
        if (answers == 2) {
            btn1.text = (answer - 2).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer - 0).toString()
        }
    }

    private fun checkButton(btn: Button, answer: String) {
        btn.setOnClickListener {
            if (answer == btn.text) {
                deleteCorrect += 1
                txtAnswer.text = "ถูกต้อง"
                txtTrue.text = "ถูก : "
                txtTrueDelete.text = "$deleteCorrect"
                btn.setBackgroundColor(Color.parseColor("#69FF78"))
            } else {
                deleteIncorrect += 1
                txtAnswer.text = "ไม่ถูกนะ"
                txtFalse.text = "ผิด : "
                txtFalseDelete.text = "$deleteIncorrect"
                btn.setBackgroundColor(Color.parseColor("#FF8848"))
            }
            Handler().postDelayed({
                btn.setBackgroundColor(Color.parseColor("#E4E1E8"))
                randomProblem()
            },250)
        }
    }

    private fun checkAnswer(answer: String){
        binding.apply {
            checkButton(btn1, answer)
            checkButton(btn2, answer)
            checkButton(btn3, answer)
        }
    }

}