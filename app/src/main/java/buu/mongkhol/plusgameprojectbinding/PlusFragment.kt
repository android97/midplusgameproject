package buu.mongkhol.plusgameprojectbinding

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import buu.mongkhol.plusgameprojectbinding.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_plus.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PlusFragment : Fragment() {

    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private var numberFirstPlus = Random.nextInt(0, 11)
    private var numberSeconPlus = Random.nextInt(0, 11)

    private lateinit var binding: FragmentPlusBinding
    // TODO: Rename and change types of parameters

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentPlusBinding>(inflater, R.layout.fragment_plus, container,false)

        binding.txtTruePlus.setTextColor(Color.parseColor("#00ff33"))
        binding.txtTrue.setTextColor(Color.parseColor("#00ff33"))
        binding.txtFalsePlus.setTextColor(Color.parseColor("#ff33cc"))
        binding.txtFalse.setTextColor(Color.parseColor("#ff33cc"))

        randomProblem()

        return binding.root
    }

    private fun randomProblem() {
        numberFirstPlus = Random.nextInt(0, 11)
        numberSeconPlus = Random.nextInt(0, 11)
        val numberFirst = binding.txtNumberFirstPlus
        val numberSecon = binding.txtNumberSeconPlus

        numberFirst.text = "$numberFirstPlus"
        numberSecon.text = "$numberSeconPlus"
        val answer = numberFirstPlus + numberSeconPlus
        val answerString = answer.toString()

        randomAnswer(binding.btn1, binding.btn2, binding.btn3, answer)
        checkAnswer(answerString)

    }

    private fun randomAnswer(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer: Int
    ) {
        val answers: Int = Random.nextInt(0, 3)
        if (answers == 0) {
            btn1.text = (answer + 0).toString()
            btn2.text = (answer + 5).toString()
            btn3.text = (answer + 2).toString()
        }
        if (answers == 1) {
            btn1.text = (answer - 1).toString()
            btn2.text = (answer + 0).toString()
            btn3.text = (answer + 2).toString()
        }
        if (answers == 2) {
            btn1.text = (answer - 2).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer + 0).toString()
        }
    }

    private fun checkButton(btn: Button, answer: String) {
        btn.setOnClickListener {
            if (answer == btn.text) {
                plusCorrect += 1
                txtAns.text = "ถูกต้อง"
                txtTrue.text = "ถูก : "
                txtTruePlus.text = "$plusCorrect"
                btn.setBackgroundColor(Color.parseColor("#69FF78"))
            } else {
                plusIncorrect += 1
                txtAns.text = "ไม่ถูกนะ"
                txtFalse.text = "ผิด : "
                txtFalsePlus.text = "$plusIncorrect"
                btn.setBackgroundColor(Color.parseColor("#FF8848"))
            }
            Handler().postDelayed({
                btn.setBackgroundColor(Color.parseColor("#E4E1E8"))
                randomProblem()
            },250)
        }
    }

    private fun checkAnswer(answer: String){
        binding.apply {
            checkButton(btn1, answer)
            checkButton(btn2, answer)
            checkButton(btn3, answer)
        }
    }

}